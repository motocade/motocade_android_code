package com.motocade.com.event_bus;

import com.motocade.com.pojo_class.home_explore_pojo.ExploreResponseDatas;
import com.motocade.com.pojo_class.likedPosts.LikedPostResponseDatas;
import com.motocade.com.pojo_class.profile_selling_pojo.ProfileSellingData;
import com.motocade.com.pojo_class.profile_sold_pojo.ProfileSoldDatas;
import com.motocade.com.pojo_class.social_frag_pojo.SocialDatas;

/**
 * <h>EventBusDatasHandler</h>
 * <p>
 *   This class acts as a mediator between the fragments while passing datas through Event Bus.
 * </p>
 * @since 26-Oct-17.
 */
public class EventBusDatasHandler
{
    private static final String TAG = EventBusDatasHandler.class.getSimpleName();

    /**
     * <h>AddSoldDatas</h>
     * <p>
     *     In this method we used to make obj of Sold Frag datas set all values of that and
     *     send that object to the SoldFrag via event bus.
     * </p>
     * @param mExploreResponseDatas The reference variables of ProfileSellingData.
     */
    public void addSoldDatas(ExploreResponseDatas mExploreResponseDatas)
    {
        ProfileSoldDatas profileSoldDatas = new ProfileSoldDatas();

        profileSoldDatas.setPostNodeId(mExploreResponseDatas.getPostNodeId());
        profileSoldDatas.setIsPromoted(mExploreResponseDatas.getIsPromoted());
        profileSoldDatas.setLikes(mExploreResponseDatas.getLikes());
        profileSoldDatas.setMainUrl(mExploreResponseDatas.getMainUrl());
        profileSoldDatas.setPlace(mExploreResponseDatas.getPlace());
        profileSoldDatas.setThumbnailImageUrl(mExploreResponseDatas.getThumbnailImageUrl());
        profileSoldDatas.setPostId(mExploreResponseDatas.getPostId());
        profileSoldDatas.setHasAudio(mExploreResponseDatas.getHasAudio());
        profileSoldDatas.setContainerHeight(mExploreResponseDatas.getContainerHeight());
        profileSoldDatas.setContainerWidth(mExploreResponseDatas.getContainerWidth());
        profileSoldDatas.setHashTags(mExploreResponseDatas.getHashTags());
        profileSoldDatas.setPostCaption(mExploreResponseDatas.getPostCaption());
        profileSoldDatas.setLatitude(mExploreResponseDatas.getLatitude());
        profileSoldDatas.setLongitude(mExploreResponseDatas.getLongitude());
        profileSoldDatas.setThumbnailUrl1(mExploreResponseDatas.getThumbnailUrl1());
        profileSoldDatas.setImageUrl1(mExploreResponseDatas.getImageUrl1());
        profileSoldDatas.setThumbnailImageUrl(mExploreResponseDatas.getThumbnailImageUrl());
        profileSoldDatas.setContainerWidth1(mExploreResponseDatas.getContainerWidth1());
        profileSoldDatas.setContainerHeight1(mExploreResponseDatas.getContainerHeight1());
        profileSoldDatas.setImageUrl2(mExploreResponseDatas.getImageUrl2());
        profileSoldDatas.setThumbnailUrl2(mExploreResponseDatas.getThumbnailUrl2());
        profileSoldDatas.setContainerHeight2(mExploreResponseDatas.getContainerHeight2());
        profileSoldDatas.setContainerWidth2(mExploreResponseDatas.getContainerWidth2());
        profileSoldDatas.setThumbnailUrl3(mExploreResponseDatas.getThumbnailUrl3());
        profileSoldDatas.setImageUrl3(mExploreResponseDatas.getImageUrl3());
        profileSoldDatas.setContainerHeight3(mExploreResponseDatas.getContainerHeight3());
        profileSoldDatas.setContainerWidth3(mExploreResponseDatas.getContainerWidth3());
        profileSoldDatas.setThumbnailUrl4(mExploreResponseDatas.getThumbnailUrl4());
        profileSoldDatas.setImageUrl4(mExploreResponseDatas.getImageUrl4());
        profileSoldDatas.setContainerHeight4(mExploreResponseDatas.getContainerHeight4());
        profileSoldDatas.setContainerWidth4(mExploreResponseDatas.getContainerWidth4());
        profileSoldDatas.setPostsType(mExploreResponseDatas.getPostsType());
        profileSoldDatas.setPostedOn(mExploreResponseDatas.getPostedOn());
        profileSoldDatas.setLikeStatus(mExploreResponseDatas.getLikeStatus());
        profileSoldDatas.setProductUrl(mExploreResponseDatas.getProductUrl());
        profileSoldDatas.setDescription(mExploreResponseDatas.getDescription());
        profileSoldDatas.setNegotiable(mExploreResponseDatas.getNegotiable());
        profileSoldDatas.setCondition(mExploreResponseDatas.getCondition());
        profileSoldDatas.setPrice(mExploreResponseDatas.getPrice());
        profileSoldDatas.setCurrency(mExploreResponseDatas.getCurrency());
        profileSoldDatas.setProductName(mExploreResponseDatas.getProductName());
        profileSoldDatas.setTotalComments(mExploreResponseDatas.getTotalComments());
        profileSoldDatas.setCategoryData(mExploreResponseDatas.getCategoryData());
        BusProvider.getInstance().post(profileSoldDatas);
    }

    /**
     * <h>AddSellingDatas</h>
     * <p>
     *     In this method we used to make obj of Selling Frag datas set all values of that and
     *     send that object to the Selling via event bus.
     * </p>
     * @param mExploreSoldDatas The reference variables of ProfileSellingData.
     */
    public void addSellingDatas(ExploreResponseDatas mExploreSoldDatas)
    {
        ProfileSellingData profileSellingData = new ProfileSellingData();
        profileSellingData.setToRemoveSellingItem(true);
        profileSellingData.setPostNodeId(mExploreSoldDatas.getPostNodeId());
        profileSellingData.setIsPromoted(mExploreSoldDatas.getIsPromoted());
        profileSellingData.setLikes(mExploreSoldDatas.getLikes());
        profileSellingData.setMainUrl(mExploreSoldDatas.getMainUrl());
        profileSellingData.setPlace(mExploreSoldDatas.getPlace());
        profileSellingData.setThumbnailImageUrl(mExploreSoldDatas.getThumbnailImageUrl());
        profileSellingData.setPostId(mExploreSoldDatas.getPostId());
        profileSellingData.setHasAudio(mExploreSoldDatas.getHasAudio());
        profileSellingData.setContainerHeight(mExploreSoldDatas.getContainerHeight());
        profileSellingData.setContainerWidth(mExploreSoldDatas.getContainerWidth());
        profileSellingData.setHashTags(mExploreSoldDatas.getHashTags());
        profileSellingData.setPostCaption(mExploreSoldDatas.getPostCaption());
        profileSellingData.setLatitude(mExploreSoldDatas.getLatitude());
        profileSellingData.setLongitude(mExploreSoldDatas.getLongitude());
        profileSellingData.setThumbnailUrl1(mExploreSoldDatas.getThumbnailUrl1());
        profileSellingData.setImageUrl1(mExploreSoldDatas.getImageUrl1());
        profileSellingData.setThumbnailImageUrl(mExploreSoldDatas.getThumbnailImageUrl());
        profileSellingData.setContainerWidth1(mExploreSoldDatas.getContainerWidth1());
        profileSellingData.setContainerHeight1(mExploreSoldDatas.getContainerHeight1());
        profileSellingData.setImageUrl2(mExploreSoldDatas.getImageUrl2());
        profileSellingData.setThumbnailUrl2(mExploreSoldDatas.getThumbnailUrl2());
        profileSellingData.setContainerHeight2(mExploreSoldDatas.getContainerHeight2());
        profileSellingData.setContainerWidth2(mExploreSoldDatas.getContainerWidth2());
        profileSellingData.setThumbnailUrl3(mExploreSoldDatas.getThumbnailUrl3());
        profileSellingData.setImageUrl3(mExploreSoldDatas.getImageUrl3());
        profileSellingData.setContainerHeight3(mExploreSoldDatas.getContainerHeight3());
        profileSellingData.setContainerWidth3(mExploreSoldDatas.getContainerWidth3());
        profileSellingData.setThumbnailUrl4(mExploreSoldDatas.getThumbnailUrl4());
        profileSellingData.setImageUrl4(mExploreSoldDatas.getImageUrl4());
        profileSellingData.setContainerHeight4(mExploreSoldDatas.getContainerHeight4());
        profileSellingData.setContainerWidth4(mExploreSoldDatas.getContainerWidth4());
        profileSellingData.setPostsType(mExploreSoldDatas.getPostsType());
        profileSellingData.setPostedOn(mExploreSoldDatas.getPostedOn());
        profileSellingData.setLikeStatus(mExploreSoldDatas.getLikeStatus());
        profileSellingData.setProductUrl(mExploreSoldDatas.getProductUrl());
        profileSellingData.setDescription(mExploreSoldDatas.getDescription());
        profileSellingData.setNegotiable(mExploreSoldDatas.getNegotiable());
        profileSellingData.setCondition(mExploreSoldDatas.getCondition());
        profileSellingData.setPrice(mExploreSoldDatas.getPrice());
        profileSellingData.setCurrency(mExploreSoldDatas.getCurrency());
        profileSellingData.setProductName(mExploreSoldDatas.getProductName());
        profileSellingData.setTotalComments(mExploreSoldDatas.getTotalComments());
        profileSellingData.setCategoryData(mExploreSoldDatas.getCategoryData());
        BusProvider.getInstance().post(profileSellingData);
    }

    /**
     * <h>SetSocialDatas</h>
     * <p>
     *     In this method we used to send the one complete object of a followed product
     *     to the socail screen through screen.
     * </p>
     * @param isToAdd the boolean flag if its true the we add the product in social screen else remove.
     */
    public void setSocialDatas(ExploreResponseDatas mExploreResponseDatas,boolean isToAdd)
    {
        try {
            SocialDatas socialDatas = new SocialDatas();
            socialDatas.setToAddSocialData(isToAdd);
            socialDatas.setPostedOn(mExploreResponseDatas.getPostedOn());
            socialDatas.setProductName(mExploreResponseDatas.getProductName());
            socialDatas.setCategoryData(mExploreResponseDatas.getCategoryData());
            socialDatas.setCurrency(mExploreResponseDatas.getCurrency());
            socialDatas.setPrice(mExploreResponseDatas.getPrice());
            socialDatas.setMemberProfilePicUrl(mExploreResponseDatas.getMemberProfilePicUrl());
            socialDatas.setMainUrl(mExploreResponseDatas.getMainUrl());
            socialDatas.setLikes(mExploreResponseDatas.getLikes());
            socialDatas.setClickCount(mExploreResponseDatas.getClickCount());
            socialDatas.setLikeStatus(mExploreResponseDatas.getLikeStatus());
            socialDatas.setPostId(mExploreResponseDatas.getPostId());
            socialDatas.setMembername(mExploreResponseDatas.getPostedByUserName());
            BusProvider.getInstance().post(socialDatas);
        }
        catch (Exception e)
        {
            System.out.println(TAG+" "+"social event bus error="+e.getMessage());
        }
    }

    /**
     * <h>AddFavouriteData</h>
     * <p>
     *     In this method we used to make a Liked pojo class instance and set all required param.
     *     Once we done it then just pass one event to pass the liked image datas to the Fav frag.
     * </p>
     */
    public void addFavouriteData(ExploreResponseDatas mExploreResponseDatas,boolean isToAdd)
    {
        LikedPostResponseDatas likedPostCategoryDatas=new LikedPostResponseDatas();
        likedPostCategoryDatas.setToAddLikedItem(isToAdd);
        likedPostCategoryDatas.setMainUrl(mExploreResponseDatas.getMainUrl());
        likedPostCategoryDatas.setProductName(mExploreResponseDatas.getProductName());
        likedPostCategoryDatas.setLikes(mExploreResponseDatas.getLikes());
        likedPostCategoryDatas.setLikeStatus(mExploreResponseDatas.getLikeStatus());
        likedPostCategoryDatas.setCurrency(mExploreResponseDatas.getCurrency());
        likedPostCategoryDatas.setPrice(mExploreResponseDatas.getPrice());
        likedPostCategoryDatas.setPostedOn(mExploreResponseDatas.getPostedOn());
        likedPostCategoryDatas.setThumbnailImageUrl(mExploreResponseDatas.getThumbnailImageUrl());
        likedPostCategoryDatas.setLikedByUsers(mExploreResponseDatas.getLikedByUsers());
        likedPostCategoryDatas.setDescription(mExploreResponseDatas.getDescription());
        likedPostCategoryDatas.setCondition(mExploreResponseDatas.getCondition());
        likedPostCategoryDatas.setPlace(mExploreResponseDatas.getPlace());
        likedPostCategoryDatas.setLatitude(mExploreResponseDatas.getLatitude());
        likedPostCategoryDatas.setLongitude(mExploreResponseDatas.getLongitude());
        likedPostCategoryDatas.setPostId(mExploreResponseDatas.getPostId());
        likedPostCategoryDatas.setPostsType(mExploreResponseDatas.getPostsType());
        likedPostCategoryDatas.setContainerWidth(mExploreResponseDatas.getContainerWidth());
        likedPostCategoryDatas.setContainerHeight(mExploreResponseDatas.getContainerHeight());
        BusProvider.getInstance().post(likedPostCategoryDatas);
    }
}
