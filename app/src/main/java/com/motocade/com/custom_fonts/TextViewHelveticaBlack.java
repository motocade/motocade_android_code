package com.motocade.com.custom_fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * <h>TextViewOrgano</h>
 * <p>
 *     In this class we used to make my custom TextView with Helvetica-Black.otf font.
 * </p>
 * @since 19-Sep-17.
 */
public class TextViewHelveticaBlack extends AppCompatTextView
{
    private static Typeface FONT_NAME;

    public TextViewHelveticaBlack(Context context) {
        super(context);
        if (FONT_NAME==null)
            FONT_NAME=Typeface.createFromAsset(context.getAssets(),"fonts/Helvetica-Black.otf");
        this.setTypeface(FONT_NAME);
    }

    public TextViewHelveticaBlack(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (FONT_NAME==null)
            FONT_NAME=Typeface.createFromAsset(context.getAssets(),"fonts/Helvetica-Black.otf");
        this.setTypeface(FONT_NAME);
    }

    public TextViewHelveticaBlack(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (FONT_NAME==null)
            FONT_NAME=Typeface.createFromAsset(context.getAssets(),"fonts/Helvetica-Black.otf");
        this.setTypeface(FONT_NAME);
    }
}
