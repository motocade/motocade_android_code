package com.motocade.com.mqttchat.Utilities;

/**
 * @since  20/06/17.
 * @author 3Embed.
 */
public class ApiOnServer
{
    public static final String CHAT_RECEIVED_THUMBNAILS_FOLDER = "/Yelo/receivedThumbnails";
    public static final String CHAT_UPLOAD_THUMBNAILS_FOLDER = "/Yelo/upload";
    public static final String CHAT_DOODLES_FOLDER = "/Yelo/doodles";
    public static final String CHAT_DOWNLOADS_FOLDER = "/Yelo/";
    public static final String IMAGE_CAPTURE_URI = "/Yelo";
    public static final String CHAT_MULTER_UPLOAD_URL = "http://ec2-18-221-37-175.us-east-2.compute.amazonaws.com:8009/";
    private static final String CHAT_UPLOAD_SERVER_URL = "http://ec2-18-221-37-175.us-east-2.compute.amazonaws.com/";
    public static final String CHAT_UPLOAD_PATH = CHAT_UPLOAD_SERVER_URL +"chat/profilePics/";
    public static final String PROFILEPIC_UPLOAD_PATH = CHAT_UPLOAD_SERVER_URL +"chat/profilePics/";
    public static final String DELETE_DOWNLOAD = CHAT_MULTER_UPLOAD_URL +"deleteImage";
    public static final String HOST = "ec2-18-221-37-175.us-east-2.compute.amazonaws.com";
    public static final String PORT = "1883";
    private static final String API_MAIN_LINK ="http://ec2-18-221-37-175.us-east-2.compute.amazonaws.com:5010/";
    public static final String TRENDING_STICKERS = "http://api.giphy.com/v1/stickers/trending?api_key=dc6zaTOxFJmzC";
    public static final String TRENDING_GIFS = "http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC";
    public static final String GIPHY_APIKEY = "&api_key=dc6zaTOxFJmzC";
    public static final String SEARCH_STICKERS = "http://api.giphy.com/v1/stickers/search?q=";
    public static final String SEARCH_GIFS = "http://api.giphy.com/v1/gifs/search?q=";
    public static final String USER_PROFILE = API_MAIN_LINK + "User/Profile";
    public static final String FETCH_CHATS = API_MAIN_LINK + "Chats";
    public static final String LOGIN_API = API_MAIN_LINK + "User/LoginWithEmail";
    public static final String VERIFY_API = API_MAIN_LINK + "User/verifyEmail";
    public static final String SIGNUP_API = API_MAIN_LINK + "User/SignupWithEmail";
    public static final String GET_USERS_API = API_MAIN_LINK + "User";
    /*
     *Mqtt user name and password */
    public static final String MQTTUSER_NAME="motocade";
    public static final String MQTTPASSWORD="gScDMD8sz3hgkU";
    /**
     * GET api to fetch the messages into the list*/
    public static final String FETCH_MESSAGES = API_MAIN_LINK + "Messages";
    /*
    * Authorization key fro chat */
    public static final String AUTH_KEY="KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfgh";

    public static final String PUSH_KEY="AIzaSyCKInBSVgbsbXK9oETs5K6mBImSoI7tqRA";
}
