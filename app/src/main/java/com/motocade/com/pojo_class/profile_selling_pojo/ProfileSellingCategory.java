package com.motocade.com.pojo_class.profile_selling_pojo;

/**
 * Created by hello on 26-Oct-17.
 */

public class ProfileSellingCategory
{
    /*"category":"electronics",
            "mainUrl":"http://138.197.65.222/public/appAssets/1496832987097.png",
            "activeImageUrl":"http://138.197.65.222/public/appAssets/1496832987098.png"*/

    private String category="",mainUrl="",activeImageUrl="";

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMainUrl() {
        return mainUrl;
    }

    public void setMainUrl(String mainUrl) {
        this.mainUrl = mainUrl;
    }

    public String getActiveImageUrl() {
        return activeImageUrl;
    }

    public void setActiveImageUrl(String activeImageUrl) {
        this.activeImageUrl = activeImageUrl;
    }
}
