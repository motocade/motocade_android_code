package com.motocade.com.pojo_class.discovery_people_pojo;

/**
 * Created by hello on 4/27/2017.
 */
public class DiscoverPeoplePostData
{
    /*"latitude":12.936578,
            "hashTags":"null",
            "containerHeight":1024,
            "thumbnailImageUrl":"http://res.cloudinary.com/yeloadmin/image/upload/v1493038600/onmbr2hcjeuhzqcdtxvt.png",
            "postedOn":1493038601542,
            "postedByUserNodeId":570,
            "longitude":80.23379799999998,
            "likes":0,
            "postsType":0,
            "postCaption":null,
            "containerWidth":768,
            "likedByUser":null,
            "postId":1493038602159,
            "postNodeId":null,
            "usersTaggedInPosts":null,
            "taggedUserCoordinates":null,
            "place":"3Edge Solutions",
            "mainUrl":"http://res.cloudinary.com/yeloadmin/image/upload/v1493038600/onmbr2hcjeuhzqcdtxvt.png",
            "hasAudio":0,
            "comments":null*/

    private String latitude="",hashTags="",containerHeight="",thumbnailImageUrl="",postedOn="",postedByUserNodeId="",longitude="",likes="",postsType="",
            postCaption="",containerWidth="",likedByUser="",postId="",postNodeId="",usersTaggedInPosts="",taggedUserCoordinates="",place="",mainUrl="",hasAudio="",comments="";

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getHashTags() {
        return hashTags;
    }

    public void setHashTags(String hashTags) {
        this.hashTags = hashTags;
    }

    public String getContainerHeight() {
        return containerHeight;
    }

    public void setContainerHeight(String containerHeight) {
        this.containerHeight = containerHeight;
    }

    public String getThumbnailImageUrl() {
        return thumbnailImageUrl;
    }

    public void setThumbnailImageUrl(String thumbnailImageUrl) {
        this.thumbnailImageUrl = thumbnailImageUrl;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public String getPostedByUserNodeId() {
        return postedByUserNodeId;
    }

    public void setPostedByUserNodeId(String postedByUserNodeId) {
        this.postedByUserNodeId = postedByUserNodeId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getPostsType() {
        return postsType;
    }

    public void setPostsType(String postsType) {
        this.postsType = postsType;
    }

    public String getPostCaption() {
        return postCaption;
    }

    public void setPostCaption(String postCaption) {
        this.postCaption = postCaption;
    }

    public String getContainerWidth() {
        return containerWidth;
    }

    public void setContainerWidth(String containerWidth) {
        this.containerWidth = containerWidth;
    }

    public String getLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(String likedByUser) {
        this.likedByUser = likedByUser;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostNodeId() {
        return postNodeId;
    }

    public void setPostNodeId(String postNodeId) {
        this.postNodeId = postNodeId;
    }

    public String getUsersTaggedInPosts() {
        return usersTaggedInPosts;
    }

    public void setUsersTaggedInPosts(String usersTaggedInPosts) {
        this.usersTaggedInPosts = usersTaggedInPosts;
    }

    public String getTaggedUserCoordinates() {
        return taggedUserCoordinates;
    }

    public void setTaggedUserCoordinates(String taggedUserCoordinates) {
        this.taggedUserCoordinates = taggedUserCoordinates;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getMainUrl() {
        return mainUrl;
    }

    public void setMainUrl(String mainUrl) {
        this.mainUrl = mainUrl;
    }

    public String getHasAudio() {
        return hasAudio;
    }

    public void setHasAudio(String hasAudio) {
        this.hasAudio = hasAudio;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
