package com.motocade.com.pojo_class.product_details_pojo;

public class ProductCategoryData
{
    private String category="",SubCategory="";

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return SubCategory;
    }

    public void setSubCategory(String subCategory) {
        SubCategory = subCategory;
    }
}
