package com.motocade.com.pojo_class.product_details_pojo;

import java.util.ArrayList;

public class ProductDetailsMain
{
    private String code="",message="";
    private ArrayList<ProductResponseDatas> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ProductResponseDatas> getData() {
        return data;
    }

    public void setData(ArrayList<ProductResponseDatas> data) {
        this.data = data;
    }
}
