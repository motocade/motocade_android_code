package com.motocade.com.pojo_class.discovery_people_pojo;

import java.util.ArrayList;

/**
 * Created by hello on 4/27/2017.
 */
public class DiscoverPeopleResponse
{
    /*"followsFlag":0,
            "postData":[],
            "postedByUserName":"header",
            "postedByUserFullName":"header",
            "profilePicUrl":"defaultUrl",
            "privateProfile":null,
            "postedByUserEmail":"qwas@gmail.com"*/

    private String followsFlag="",postedByUserName="",postedByUserFullName="",profilePicUrl="",privateProfile="",postedByUserEmail="";
    private ArrayList<DiscoverPeoplePostData> postData;

    public String getFollowsFlag() {
        return followsFlag;
    }

    public void setFollowsFlag(String followsFlag) {
        this.followsFlag = followsFlag;
    }

    public String getPostedByUserName() {
        return postedByUserName;
    }

    public void setPostedByUserName(String postedByUserName) {
        this.postedByUserName = postedByUserName;
    }

    public String getPostedByUserFullName() {
        return postedByUserFullName;
    }

    public void setPostedByUserFullName(String postedByUserFullName) {
        this.postedByUserFullName = postedByUserFullName;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getPrivateProfile() {
        return privateProfile;
    }

    public void setPrivateProfile(String privateProfile) {
        this.privateProfile = privateProfile;
    }

    public String getPostedByUserEmail() {
        return postedByUserEmail;
    }

    public void setPostedByUserEmail(String postedByUserEmail) {
        this.postedByUserEmail = postedByUserEmail;
    }

    public ArrayList<DiscoverPeoplePostData> getPostData() {
        return postData;
    }

    public void setPostData(ArrayList<DiscoverPeoplePostData> postData) {
        this.postData = postData;
    }
}
