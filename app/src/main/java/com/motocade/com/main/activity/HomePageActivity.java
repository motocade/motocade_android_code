package com.motocade.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import com.motocade.com.R;
import com.motocade.com.fcm_push_notification.Config;
import com.motocade.com.fcm_push_notification.NotificationMessageDialog;
import com.motocade.com.fcm_push_notification.NotificationUtils;
import com.motocade.com.main.tab_fragments.ChatFrag;
import com.motocade.com.main.tab_fragments.HomeFrag;
import com.motocade.com.main.tab_fragments.ProfileFrag;
import com.motocade.com.main.tab_fragments.SocialFrag;
import com.motocade.com.utility.CommonClass;
import com.motocade.com.utility.DialogBox;
import com.motocade.com.utility.SessionManager;
import com.motocade.com.utility.VariableConstants;
import java.lang.reflect.Field;

/**
 * <h>HomePageActivity</h>
 * <p>
 *     In this class we have the bottom Navigation layout for various fragment transition.
 * </p>
 * @since 3/31/2017
 */
public class HomePageActivity extends AppCompatActivity
{
    private static final String TAG = HomePageActivity.class.getSimpleName();
    public VerifyLoginWithFacebook verifyLoginWithFacebook;
    private Activity mActivity;
    private SessionManager mSessionManager;
    private Fragment homeFrag,socialFarg,chatFarg,myProfileFrag;
    private boolean isToHighLightTab;
    private NotificationMessageDialog mNotificationMessageDialog;
    private BottomNavigationView bottomNavigationView;
    public RelativeLayout rL_rootElement;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

        mActivity=HomePageActivity.this;
        isToHighLightTab=false;
        mSessionManager=new SessionManager(mActivity);
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);
        rL_rootElement = (RelativeLayout) findViewById(R.id.rL_rootElement);

        // receive datas
        Intent intent = getIntent();
        if (intent!=null)
        {
            boolean isFromSignup = intent.getBooleanExtra("isFromSignup",false);
            System.out.println(TAG+" "+"isFromSignup first="+isFromSignup);

            if (isFromSignup)
                new DialogBox(mActivity).startBrowsingDialog();
        }
        initBottomNavView();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.containsKey("openChat")&&bundle.getBoolean("openChat"))
            {
                if(CommonClass.isBackFromChat)
                {
                    CommonClass.isBackFromChat=false;
                    View view = bottomNavigationView.findViewById(R.id.nav_chat);
                    view.performClick();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * <h>InitBottomNavView</h>
     * <p>
     *     In this method we used to initialize BottomNavigationView variables.
     * </p>
     */
    private void initBottomNavView()
    {
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        disableShiftMode(bottomNavigationView);

        homeFrag=new HomeFrag();
        socialFarg=new SocialFrag();
        chatFarg=new ChatFrag();
        myProfileFrag=new ProfileFrag();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        final Menu nav_menu = bottomNavigationView.getMenu();
        nav_menu.getItem(0).setIcon(R.drawable.tabbar_home_onn);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item)
                    {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        switch (item.getItemId())
                        {
                            // Home frag
                            case R.id.nav_home:
                                setBottomTabIcon(nav_menu,R.drawable.tabbar_home_onn,R.drawable.tabbar_social_off,R.drawable.tab_bar_chat_off,R.drawable.tabbar_profile_off);
                                System.out.println(TAG+" "+"home frag="+homeFrag.isAdded());
                                isToHighLightTab=true;

                                if (homeFrag.isAdded())
                                    transaction.show(homeFrag);
                                else
                                    transaction.add(R.id.frame_layout,homeFrag,"HomeFragment");
                                if (socialFarg.isAdded())
                                    transaction.hide(socialFarg);
                                if (chatFarg.isAdded())
                                    transaction.hide(chatFarg);
                                if (myProfileFrag.isAdded())
                                    transaction.hide(myProfileFrag);
                                break;

                            // Social
                            case R.id.nav_social:
                                if (mSessionManager.getIsUserLoggedIn())
                                {
                                    setBottomTabIcon(nav_menu,R.drawable.tabbar_home_off,R.drawable.tabbar_social_on,R.drawable.tab_bar_chat_off,R.drawable.tabbar_profile_off);
                                    isToHighLightTab=true;
                                    if (socialFarg.isAdded())
                                        transaction.show(socialFarg);
                                    else transaction.add(R.id.frame_layout,socialFarg,"SocialFrag");
                                    if (chatFarg.isAdded())
                                        transaction.hide(chatFarg);
                                    if (myProfileFrag.isAdded())
                                        transaction.hide(myProfileFrag);
                                    if (homeFrag.isAdded())
                                        transaction.hide(homeFrag);
                                }
                                else startActivityForResult(new Intent(mActivity,LandingActivity.class), VariableConstants.LANDING_REQ_CODE);
                                break;

                            // Camera
                            case R.id.nav_camera:
                                //item.setIcon(R.drawable.tab_bar_camera_on);
                                isToHighLightTab=false;
                                if (mSessionManager.getIsUserLoggedIn())
                                {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                        startActivity(new Intent(mActivity, Camera2Activity.class));
                                    else
                                        startActivity(new Intent(mActivity, CameraActivity.class));
                                }
                                else startActivityForResult(new Intent(mActivity,LandingActivity.class), VariableConstants.LANDING_REQ_CODE);
                                break;

                            // chat
                            case R.id.nav_chat:
                                if (mSessionManager.getIsUserLoggedIn())
                                {
                                    setBottomTabIcon(nav_menu,R.drawable.tabbar_home_off,R.drawable.tabbar_social_off,R.drawable.tab_bar_chat_onn,R.drawable.tabbar_profile_off);
                                    isToHighLightTab=true;

                                    if (chatFarg.isAdded())
                                        transaction.show(chatFarg);
                                    else transaction.add(R.id.frame_layout,chatFarg);
                                    if (socialFarg.isAdded())
                                        transaction.hide(socialFarg);
                                    if (myProfileFrag.isAdded())
                                        transaction.hide(myProfileFrag);
                                    if (homeFrag.isAdded())
                                        transaction.hide(homeFrag);
                                }
                                else startActivityForResult(new Intent(mActivity,LandingActivity.class), VariableConstants.LANDING_REQ_CODE);
                                break;

                            // My profile
                            case R.id.nav_profile:
                                if (mSessionManager.getIsUserLoggedIn())
                                {
                                    setBottomTabIcon(nav_menu,R.drawable.tabbar_home_off,R.drawable.tabbar_social_off,R.drawable.tab_bar_chat_off,R.drawable.tabbar_profile_on);
                                    isToHighLightTab=true;

                                    if (myProfileFrag.isAdded())
                                        transaction.show(myProfileFrag);
                                    else transaction.add(R.id.frame_layout,myProfileFrag);
                                    if (socialFarg.isAdded())
                                        transaction.hide(socialFarg);
                                    if (chatFarg.isAdded())
                                        transaction.hide(chatFarg);
                                    if (homeFrag.isAdded())
                                        transaction.hide(homeFrag);
                                }
                                else startActivityForResult(new Intent(mActivity,LandingActivity.class), VariableConstants.LANDING_REQ_CODE);
                                break;
                        }
                        transaction.commit();

                        return isToHighLightTab;
                    }
                });
        transaction.add(R.id.frame_layout, homeFrag);
        transaction.commit();
    }

    /**
     * <h>SetBottomTabIcon</h>
     * <p>
     *     In this method we used to set the selected and unselected icon of bottom navigation view.
     * </p>
     * @param nav_menu The Navigation Menu
     * @param homeIcon First Tab i.e HomePage icon
     * @param socialIcon The Second Tab i.e SocialPage icon
     * @param chatIcon The Fourth Tab i.e Camera Page icon
     * @param profileIcon The Fifth Tab i.e Profile page icon
     */
    private void setBottomTabIcon(Menu nav_menu,int homeIcon,int socialIcon,int chatIcon,int profileIcon)
    {
        nav_menu.getItem(0).setIcon(homeIcon);
        nav_menu.getItem(1).setIcon(socialIcon);
        nav_menu.getItem(3).setIcon(chatIcon);
        nav_menu.getItem(4).setIcon(profileIcon);
    }

    /**
     * <h>DisableShiftMode</h>
     * <p>
     *     Method for disabling ShiftMode of BottomNavigationView
     * </p>
     * @param view The BottomNavigationView
     */
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(TAG+" "+"onactivity result "+"res code="+resultCode+" "+"req code="+requestCode+" "+"data="+data);
        if (data!=null)
        {
            switch (requestCode)
            {
                case VariableConstants.LANDING_REQ_CODE :
                    boolean isToRefreshHomePage = data.getBooleanExtra("isToRefreshHomePage",false);
                    boolean isFromSignup = data.getBooleanExtra("isFromSignup",false);
                    System.out.println(TAG+" "+"isToRefreshHomePage="+isToRefreshHomePage+" "+"isFromSignup="+isFromSignup);
                    if (isToRefreshHomePage)
                    {
                        /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.remove(homeFrag);
                        transaction.add(R.id.frame_layout,homeFrag,"HomeFragment");
                        transaction.show(homeFrag);
                        transaction.commit();*/

                        Intent intent = getIntent();
                        intent.putExtra("isFromSignup",isFromSignup);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }

            if ( verifyLoginWithFacebook!=null)
                verifyLoginWithFacebook.fbOnActivityResult(requestCode,resultCode,data);

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}