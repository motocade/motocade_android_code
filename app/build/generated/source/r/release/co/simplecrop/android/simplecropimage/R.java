/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package co.simplecrop.android.simplecropimage;

public final class R {
    public static final class color {
        public static final int black = 0x7f0d0010;
        public static final int crop__button_bar = 0x7f0d004a;
        public static final int crop__button_text = 0x7f0d004b;
        public static final int crop__selector_focused = 0x7f0d004c;
        public static final int crop__selector_pressed = 0x7f0d004d;
        public static final int white = 0x7f0d00d4;
    }
    public static final class dimen {
        public static final int crop__bar_height = 0x7f090070;
        public static final int text_size = 0x7f0900d0;
        public static final int twenty_five_dp = 0x7f0900d6;
    }
    public static final class drawable {
        public static final int camera_crop_height = 0x7f020069;
        public static final int camera_crop_width = 0x7f02006a;
        public static final int ic_rotate_left = 0x7f020471;
        public static final int indicator_autocrop = 0x7f020477;
        public static final int rotate_icon = 0x7f0204f5;
    }
    public static final class id {
        public static final int image = 0x7f0f006f;
        public static final int rL_bottom_layout = 0x7f0f023f;
        public static final int rL_cancel = 0x7f0f0240;
        public static final int rL_done = 0x7f0f00a0;
        public static final int rotateLeft = 0x7f0f0241;
    }
    public static final class layout {
        public static final int cropimage = 0x7f03005e;
        public static final int main = 0x7f03009d;
    }
    public static final class string {
        public static final int cancel = 0x7f08012c;
        public static final int crop__cancel = 0x7f080152;
        public static final int crop__done = 0x7f080153;
        public static final int done = 0x7f08016d;
        public static final int no_storage_card = 0x7f0801da;
        public static final int not_enough_space = 0x7f0801dc;
        public static final int preparing_card = 0x7f080218;
        public static final int saving_image = 0x7f080242;
    }
    public static final class style {
        public static final int Crop = 0x7f0a00dd;
        public static final int Crop_ActionButtonText = 0x7f0a00de;
    }
}
