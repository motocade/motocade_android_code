package com.suresh.innapp_purches;

import java.util.ArrayList;
import java.util.Collections;
/**
 * @since  15/4/16.
 */
public class InAppConstants
{
    //changed
    public static final String base64EncodedPublicKey ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAozgKNldR6zi0WLSsQwOR79jjmDtDjyFlpifNcrGU/qGCs2kTmNVz7tIfByewhtoio5X3E2m3lhQtRuvAiKzAo6niq7Hqa0pxntOMBs1IrrcvRbhK8g/yCNCS9e/ymV7izRqqPWqWgQlwS/e6R56UwRrh9IWvKEE3GOCb8JX98LXR2GPwTIEqc5LhmhTRDBL6k2OUFD2hmXHF9TkRDQIKrFkmXvhHyov4gXrU0xpdBsKk+QlQUUN+MN3bGrxW5WkcXkrPP+j0w2esqpjq+RhOGjfCXwSzkyCYatB9TibevQw1NJ6/oU8uYzvFmoEp3lDd0keZtTnAmnBaXqE4rdnCTQIDAQAB".trim();
    /*
     *purchase item details. */
    public enum Purchase_item
    {
        LOWER_PRICE("com.motocade.100Clicks","1503151128429"),
        MEDIUM_PRICE("com.motocade.200Clicks","1503153105155"),
        HIGH_PRICE("com.motocade.300Clicks","1504019528923"),
        HIGEST_PRICE("com.motocade.500Clicks","1499918013413"),
        TEST_CASES("com.test.purchased","");

        public static Purchase_item getPurchaseItem(String id)
        {
            Purchase_item purchase_item=null;
            ArrayList<Purchase_item> purchase_items = new ArrayList<>();
            Collections.addAll(purchase_items,Purchase_item.values());
            for(Purchase_item item:purchase_items)
            {
                if(item.getKey().equals(id))
                {
                    purchase_item=item;
                    break;
                }
            }
            if(purchase_item==null)
            {
                purchase_item=TEST_CASES;
            }
            return purchase_item;
        }
        /*
         *Getting the plan Id */
        public static Purchase_item getPlanId(String playKey)
        {
            Purchase_item purchase_item=null;
            ArrayList<Purchase_item> purchase_items = new ArrayList<>();
            Collections.addAll(purchase_items,Purchase_item.values());
            for(Purchase_item item:purchase_items)
            {
                if(item.getKey().equals(playKey))
                {
                    purchase_item=item;
                    break;
                }
            }
            return purchase_item;
        }

        public static ArrayList<String> getPurchaseItemList()
        {
            ArrayList<String> temp_list=new ArrayList<>();
            Purchase_item purchase_item=null;
            ArrayList<Purchase_item> purchase_items = new ArrayList<>();
            Collections.addAll(purchase_items,Purchase_item.values());
            for(Purchase_item item:purchase_items)
            {
                temp_list.add(item.getKey());
            }
            return temp_list;
        }

        private String value;
        private String id;
        public String getId()
        {
            return this.id;
        }
        public String getKey()
        {
            return this.value;
        }
        Purchase_item(String value,String id)
        {
            this.value = value;
            this.id=id;
        }
    }

}
